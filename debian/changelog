libcgi-xmlapplication-perl (1.1.5-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Fabrizio Regalli from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/*: update URLs from {search,www}.cpan.org to MetaCPAN.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 10 Jun 2022 15:42:03 +0100

libcgi-xmlapplication-perl (1.1.5-1) unstable; urgency=medium

  * Team upload.
  * Add debian/upstream/metadata
  * Import upstream version 1.1.5
  * Remove patch that fixed spelling errors. Is is already applied by
    upstream.

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Thu, 20 Aug 2015 07:10:52 -0300

libcgi-xmlapplication-perl (1.1.4-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.1.4
  * Update spelling error patch with new ones
  * Remove debian/tests/pkg-perl/module-name

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Mon, 17 Aug 2015 08:51:13 -0300

libcgi-xmlapplication-perl (1.1.3-8) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.
  * Add explicit (build) dependency on libcgi-pm-perl.
  * Mention module name in long description.
  * Extend and forward 01-fix-spelling-error.diff.

 -- gregor herrmann <gregoa@debian.org>  Sat, 13 Jun 2015 12:29:42 +0200

libcgi-xmlapplication-perl (1.1.3-7) unstable; urgency=low

  * Team upload

  [ gregor herrmann ]
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed: XS-
    Vcs-Svn fields.
  * debian/watch: use dist-based URL.
  * Drop POD fixes, not needed any more.
  * Set Standards-Version to 3.7.3 (no changes).
  * Refresh debian/rules, no functional changes.
  * Delete debian/libcgi-xmlapplication-perl.*, let debian/rules do the work.
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).
  * debian/control: Added: ${misc:Depends} to Depends: field.

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ gregor herrmann ]
  * Change my email address.
  * debian/control: Changed: (build-)depend on perl instead of perl-
    modules.

  [ Fabrizio Regalli ]
  * Bump to 3.9.2 Standard-Version.
  * Switch to DEP5 license format.
  * Added myself to Uploaders and Copyright.
  * Added 01-fix-spelling-error.diff.
  * Moved d/rules to three lines standard version.
  * Bump to 3.0 quilt format.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"
  * Apply wrap-and-sort
  * Fix lintian warning binary-control-field-duplicates-source (2x)
  * Bump debhelper compatibility to 9
    + Update versioned debhelper build-dependency accordingly
  * Bump Standards-Version to 3.9.5 (no changes)

 -- Axel Beckert <abe@debian.org>  Sat, 14 Dec 2013 03:02:09 +0100

libcgi-xmlapplication-perl (1.1.3-6) unstable; urgency=low

  * New Maintainer: Debian Perl Group (closes: #317405).
  * Improve watch file.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Fri,  6 Oct 2006 20:46:45 +0200

libcgi-xmlapplication-perl (1.1.3-5) unstable; urgency=low

  * QA upload.
  * Use $(CURDIR) instead of $(PWD) in debian/rules (Closes: #390475)
  * Move debhelper to B-D-I
  * Use at least debhelper 5.0.0
  * Conforms with latest Standards Version 3.7.2

 -- Michael Ablassmeier <abi@debian.org>  Sun,  1 Oct 2006 17:29:02 +0200

libcgi-xmlapplication-perl (1.1.3-4) unstable; urgency=low

  * Set maintainer to Debian QA Group
  * Updated Standards-Version
  * Removed versions in versioned dependencies as even oldstable has high enough
    versions

 -- Luk Claes <luk@debian.org>  Sat, 10 Sep 2005 11:53:34 +0000

libcgi-xmlapplication-perl (1.1.3-3) unstable; urgency=low

  * Changing maintainer to Debian Perl Group
  * debian/control: added Uploaders
  * XMLApplication.pm: escaping some - as \-

 -- Luk Claes <luk@debian.org>  Tue, 18 Jan 2005 15:55:06 +0100

libcgi-xmlapplication-perl (1.1.3-2) unstable; urgency=high

  * Urgency high because of FTBFS
  * Makefile: rerun Makefile instead of failing.

 -- Luk Claes <luk.claes@ugent.be>  Fri,  5 Nov 2004 16:51:22 +0100

libcgi-xmlapplication-perl (1.1.3-1) unstable; urgency=low

  * New upstream release (closes: #252929)
  * debian/watch: added
  * debian/copyright: added me as maintainer

 -- Luk Claes <luk.claes@ugent.be>  Mon,  7 Jun 2004 17:20:14 +0200

libcgi-xmlapplication-perl (1.1.2-2) unstable; urgency=low

  * New maintainer (closes: #210207)
  * Uploaded for Luk by Jaldhar H. Vyas <jaldhar@debian.org>

 -- Jaldhar H. Vyas <jaldhar@debian.org>  Wed, 22 Oct 2003 19:48:37 +0000

libcgi-xmlapplication-perl (1.1.2-1) unstable; urgency=low

  * New upstream release
  * debian/control: removed obsolete (build) dependency on 'libcgi-pm-perl'
  * debian/rules: moved debhelper compatibility level setting to
    'debian/compat' per latest debhelper best practices
  * debian/control: updated sections according to latest archive changes:
    - 'libcgi-xmlapplication-perl' from 'interpreters' to 'perl'
  * debian/control: changed build dependency on 'debhelper' to '(>= 4.1)'
  * debian/control: upgraded to Debian Policy 3.6.0 (no changes)

 -- Ardo van Rangelrooij <ardo@debian.org>  Sat, 26 Jul 2003 12:33:39 -0500

libcgi-xmlapplication-perl (1.1.1-2) unstable; urgency=low

  * debian/rules: upgraded to debhelper v4
  * debian/control: changed build dependency on debhelper accordingly
  * debian/rules: migrated from 'dh_movefiles' to 'dh_install'
  * debian/rules: split off 'install' target from 'binary-indep' target
  * debian/copyright: added pointer to license

 -- Ardo van Rangelrooij <ardo@debian.org>  Sun,  4 Aug 2002 20:15:32 -0500

libcgi-xmlapplication-perl (1.1.1-1) unstable; urgency=low

  * New upstream release

 -- Ardo van Rangelrooij <ardo@debian.org>  Tue,  1 Jan 2002 13:48:08 -0600

libcgi-xmlapplication-perl (1.1.0-1) unstable; urgency=low

  * New upstream release

 -- Ardo van Rangelrooij <ardo@debian.org>  Sat, 22 Dec 2001 20:17:51 -0600

libcgi-xmlapplication-perl (1.0-1) unstable; urgency=low

  * New upstream release
  * debian/control: upgraded to Debian Policy 3.5.6

 -- Ardo van Rangelrooij <ardo@debian.org>  Sat, 17 Nov 2001 20:25:24 -0600

libcgi-xmlapplication-perl (0.9.3-2) unstable; urgency=low

  * debian/control: reduced description to its essentials
    (closes: Bug#111807)

 -- Ardo van Rangelrooij <ardo@debian.org>  Thu, 13 Sep 2001 21:55:39 -0500

libcgi-xmlapplication-perl (0.9.3-1) unstable; urgency=low

  * Initial Release

 -- Ardo van Rangelrooij <ardo@debian.org>  Sat,  1 Sep 2001 21:08:48 -0500
